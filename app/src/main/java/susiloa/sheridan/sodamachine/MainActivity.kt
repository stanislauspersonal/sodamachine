package susiloa.sheridan.sodamachine

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.RadioButton
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.io.Serializable

class MainActivity : AppCompatActivity() {

    var cherrySyrup = Syrup("Cherry", 100.0);
    var strawberrySyrup = Syrup("Strawberry", 100.0);
    var lemonSyrup = Syrup("Lemon", 100.0);
    var water = Water( 10000.0);
    var co2 = Co2(100.0);

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        buttonDispense.setOnClickListener{
            // Get the checked radio button id from radio group
            var syrupId: Int = syrupGroup.checkedRadioButtonId
            var syrup: Syrup?;
            if (syrupId == R.id.radioCherry){
                syrup = cherrySyrup
            } else if (syrupId == R.id.radioStrawberry) {
                syrup = strawberrySyrup
            } else {
                syrup = null;
            }

            var sizeId: Int = sizeGroup.checkedRadioButtonId
            var size: Int = -1;
            if (sizeId == R.id.smallRadio) {
                size = 0;
            } else if (sizeId == R.id.mediumRadio) {
                size = 1;
            }else if (sizeId == R.id.largeRadio) {
                size = 2;
            }

            if(size == -1) {
                Toast.makeText(applicationContext, "Please Choose a size", Toast.LENGTH_LONG).show();
            } else {
                dispenseDrink(syrup, size)
            }
        }

        btnOpen.setOnClickListener() {
            openMachine();
        }
    }
    fun openMachine() {
        val i = Intent(this, InternalMachine::class.java)
        i.putExtra("lemon", lemonSyrup.getLevel())
        i.putExtra("cherry", cherrySyrup.getLevel())
        i.putExtra("strawberry", strawberrySyrup.getLevel())
        i.putExtra("water", water.getLevel())
        i.putExtra("co2", co2.getLevel())
        startActivityForResult(i, 1);
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 1){
            var response = data!!.getIntExtra("response", 5)
            when(response) {
                0 -> lemonSyrup.refill()
                1 -> cherrySyrup.refill()
                2 -> strawberrySyrup.refill()
                3 -> water.refill()
                4 -> co2.refill()
            }
            if(response != 5) {
                openMachine()
            }
        }
    }

    fun dispenseDrink (syrup : Syrup?, size : Int) {
        var error = ""
        if(syrup != null) {
            if(!syrup.checkEnough(size)) {
                error += "Not enough ${syrup.name} syrup \n"
            }
        }
        if(!lemonSyrup.checkEnough(size)) {
            error += "Not enough Lemon syrup" + "\n"
        }

        if(!water.checkEnough(size)) {
            error += "Not enough water" + "\n"
        }

        if(!co2.checkEnough(size)) {
            error += "Not enough Co2" + "\n"
        }

        if(error.length != 0) {
            Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show();
        } else {
            var syrupName = "";
            if(syrup != null) {
                syrupName = syrup.name
                syrup.dispenseSyrup(size)
            }
            lemonSyrup.dispenseSyrup(size)
            water.dispenseWater(size)
            co2.dispenseCo2(size)

            var sizeText = "";
            when(size) {
                0 -> sizeText = "small"
                1 -> sizeText = "medium"
                2 -> sizeText = "large"
            }

            Toast.makeText(applicationContext, "Dispensing $sizeText $syrupName Lemon Soda....", Toast.LENGTH_LONG).show();
        }
    }
}

class SyrupPump(var co2Pressure: Double, var syrup: Syrup) {
    var syrupSensor: Boolean = true;

    public fun syrupIsEmpty(): Boolean {
        return syrupSensor;
    }

    public fun dispenseSyrup() {

    }
}

class Syrup(val name: String, var level: Double) : Serializable {
    var isEmpty = false;
    fun dispenseSyrup(size: Int) : String {
        if (level < (size + 1)*7.1) {
            return "Not enough $name syrup"
        } else {
            level -= size * 7.1;
            if (level < 7.1) {
                isEmpty = true;
            }
            return "success";
        }
    }

    fun checkEnough(size: Int) : Boolean {
        return level >= (size + 1)*7.1
    }

    fun refill() {
        level = 100.0;
        isEmpty = false;
    }

    fun getLevel() : Int {
        return (level / 100 * 100).toInt();
    }
}

class Water (var waterLevel: Double) : Serializable {
    var isEmpty = false;
    public fun waterIsEmpty() : Boolean {

        return false;
    }
    fun dispenseWater(size: Int): String {
        if (waterLevel < (size + 1)*300) {
            return "Not enough Water"
        } else {
            waterLevel -= size * 300;
            if (waterLevel < 300) {
                isEmpty = true;
            }
            return "success";
        }
    }

    fun checkEnough(size: Int) : Boolean {
        return waterLevel >= (size + 1)*300
    }

    fun refill() {
        waterLevel = 10000.0;
        isEmpty = false;
    }

    fun getLevel() : Int {
        return (waterLevel / 10000 * 100).toInt();
    }
}

class WaterPump (var water : Water, var pumpPressure : Double) {

    public fun boostWater() {

    }
}

class Carbonator (var water : WaterPump, var co2Level : Double) {

    public fun dispenseCarbonatedWater() {

    }
}

class Co2 (var co2Pressure : Double) : Serializable {
    var isEmpty : Boolean = false;
    public fun dispenseCo2(size : Int) : String {
        if (co2Pressure < (size + 1)*10) {
            return "Not enough Co2"
        } else {
            co2Pressure -= size * 10;
            if (co2Pressure < 10) {
                isEmpty = true;
            }
            return "success";
        }
    }

    public fun co2IsEmpty() : Boolean {

        return isEmpty;
    }

    fun checkEnough(size: Int) : Boolean {
        return co2Pressure >= (size + 1)*10
    }

    fun refill() {
        co2Pressure = 100.0;
        isEmpty = false;
    }

    fun getLevel(): Int {
        return (co2Pressure / 100 * 100).toInt()
    }
}

class LowRegulator (var syrupPump : SyrupPump) {

    public fun dispenseCo2() {

    }
}

class MainRegulator (var mainRegulator : Carbonator, var lowRegulator: LowRegulator, var co2 : Co2, var sensorCo2 : Boolean) {

    public fun pressurizeCarbonator() {

    }

    public fun pressurizeLowRegulator() {

    }
}

class ColdPlates (var coWaterDispenser : Carbonator, var syrups : Array<SyrupPump>) {

    public fun prepareDrink() {

    }
}

class Dispenser (var syrups : Array<String>, var cupSize : String, var drinkSelected : String, var mixAndDispense : ColdPlates) {

}