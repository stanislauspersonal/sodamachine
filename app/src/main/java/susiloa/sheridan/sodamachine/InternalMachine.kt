package susiloa.sheridan.sodamachine

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_internal_machine.*

class InternalMachine : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_internal_machine)

        mainPressureText.text = (40..70).random().toString() + " psi"
        lowPressureText.text = (0..14).random().toString() + " psi"
        coldPlateTemp.text = (-1..15).random().toString() + " °C"


        lemonLevel.progress = intent.getIntExtra("lemon", 0)
        cherryLevel.progress = intent.getIntExtra("cherry", 0)
        strawberryLevel.progress = intent.getIntExtra("strawberry", 0)
        waterLevel.progress = intent.getIntExtra("water", 0)
        co2Level.progress = intent.getIntExtra("co2", 0)

        lemonBtn.setOnClickListener(){
            sendResponse(0)
        }

        cherryBtn.setOnClickListener(){
            sendResponse(1)
        }

        strawberryBtn.setOnClickListener(){
            sendResponse(2)
        }

        waterBtn.setOnClickListener(){
            sendResponse(3)
        }

        co2Btn.setOnClickListener(){
            sendResponse(4)
        }

        closeBtn.setOnClickListener(){
            sendResponse(5)
        }
    }

    fun sendResponse(code: Int) {
        var i = Intent();
        intent.putExtra("response", code)
        setResult(1, intent)
        finish()
    }
}